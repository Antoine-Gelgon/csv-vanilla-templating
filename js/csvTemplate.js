var csv_file  = document.getElementById('csvTemplate').getAttribute('data-csv')
var csv_separator  = document.getElementById('csvTemplate').getAttribute('data-separator')
var csv

function slugify(str) {
  return String(str)
    .normalize('NFKD')
    .replace(/[\u0300-\u036f]/g, '')
    .trim()
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, '')
    .replace(/\s+/g, '-')
    .replace(/-+/g, '-');
}

function shuffle(array) {
  let currentIndex = array.length;

  while (currentIndex != 0) {

    let randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }
}


function buildStatic(elem, i, row){
	var dataAttrs = Array.prototype.slice.call(elem.attributes)
	dataAttrs.forEach(function(attrs){
		if (attrs.nodeName.startsWith("data")){
			var expr = attrs.nodeName
			var regExp = /\(([^)]+)\)/

			if (regExp.test(attrs.nodeValue)) {
				var matches = regExp.exec(attrs.nodeValue);
				var opt = matches[1]
				var nodeValue = attrs.nodeValue.replace(matches[0], '') 
			}else {
				var nodeValue = attrs.nodeValue
			}

			if (row[nodeValue]) {
				var val = row[nodeValue]
			} else {
				var val = nodeValue
			}

			if (typeof val !== 'undefined') {
				if(expr == "data-classname"){ elem.classList.add(slugify(val)) }
				else if(expr == "data-id" || expr == "data-href"){
					if (typeof opt !== 'undefined') {
						if(opt == "index") {
							val = val + i 
						}
					}
					elem.setAttribute(expr.split('-')[1], val)
				}
				else if(expr == "data-content"){ elem.innerHTML = val }
				else {
					var newName = expr.split('-')[1]
					try {
						val = decodeURI(val)
					} catch (error) {
						console.log('error val encodeURI')
					}
					elem.setAttribute(newName, val)
				}
			} else { console.log(elem, 'none') }	
		}
	})
}

function loopAction(csv, LOOP){
	
	if (LOOP.dataset.range) {
		var sp = LOOP.dataset.range.split('-')
		var start = sp[0]; var end = sp[1]
		var csv = csv.slice(start, end)
	}

	if (LOOP.dataset.sort) {
		if (LOOP.dataset.sort == 'random') { shuffle(csv) }
	}

	var LOOP_TPL = LOOP.querySelector('*')
	var LC = LOOP.querySelectorAll('[data-content], [data-src], [data-class], [data-href], [data-style],[data-id], [data-classname], [data-tags]')

	LOOP.innerHTML = "" 

	csv.forEach(function(row, i){
		LC.forEach(function(elem, ii){
			buildStatic(elem, i, csv[i])
		})
		LOOP.innerHTML += LOOP_TPL.outerHTML 
	})
}

function itemAction(row, ITEM){
	var ITEM_TPL = ITEM.querySelector('*')
	var IC = ITEM.querySelectorAll('[data-content], [data-src], [data-class], [data-href], [data-style],[data-id], [data-classname], [data-tags]')

	IC.forEach(function(elem, ii){
		buildStatic(elem, 0, row)
	})
}

async function getAPI() {
	let response = await fetch(csv_file);
	let data = await response.text();
	return data;
}

function sortCSV(csv, entry, value) {
	var rr = []
	console.log(typeof(csv))
	console.log(typeof(rr))
	csv.forEach(function(row, i){
		if(row[entry] == value) {
			console.log(row[entry])
			rr.push(row)
		}
	})

	return rr
}

function csvTemplate(csv) {
	var LOOPS = document.querySelectorAll('[data-loop="true"]')
	var ITEMS = document.querySelectorAll('[data-item]')
	
	const searchParams = new URLSearchParams(window.location.search)
	if (searchParams.has('page')) {
		var PAGES = document.querySelectorAll('[data-page]')
		var page_num = searchParams.get('page')
		PAGES.forEach(function(PAGE, i){
			itemAction(csv[page_num], PAGE)
		})
	} else if (searchParams.size > 0) {
		if (searchParams.has('TAGS')) {
			csv =	sortCSV(csv, "TAGS", )
		}
	}

	ITEMS.forEach(function(ITEM, i){
		itemAction(csv[ITEM.dataset.item], ITEM)
	})
	LOOPS.forEach(function(LOOP, i){
		loopAction(csv, LOOP)
	})
	
}

getAPI().then(data => {
	var csv = parseCSV(data, csv_separator)
	csvTemplate(csv)
})	

function parseCSV(strData, strDelimiter) {
	var array_csv = CSVToArray(strData, strDelimiter)
	var keys =  array_csv[0]
	array_csv.shift()
	array_csv.pop()
	var obj_csv = []
	array_csv.forEach(function(values,i){
		var obj = {} 
		keys.forEach(function(key,u) {
			obj[key] = values[u]
		})
		obj_csv.push(obj);

	})
	return obj_csv 
}

function CSVToArray( strData, strDelimiter ){
	strDelimiter = (strDelimiter || ",");

	var objPattern = new RegExp(
		(
			"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

			"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

			"([^\"\\" + strDelimiter + "\\r\\n]*))"
		),
		"gi"
	);


	var arrData = [[]];

	var arrMatches = null;


	while (arrMatches = objPattern.exec( strData )){

		var strMatchedDelimiter = arrMatches[ 1 ];

		if (
			strMatchedDelimiter.length &&
			(strMatchedDelimiter != strDelimiter)
		){
			arrData.push( [] );
		}

		if (arrMatches[ 2 ]){

			var strMatchedValue = arrMatches[ 2 ].replace(
				new RegExp( "\"\"", "g" ),
				"\""
			);

		} else {

			var strMatchedValue = arrMatches[ 3 ];

		}
		arrData[ arrData.length - 1 ].push( strMatchedValue );
	}

	return( arrData );
}
